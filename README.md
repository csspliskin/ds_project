# Airfare prediction and classification

A machine learning exercise presenting a simple model for

* (1) prediction of airfares from the data contained in the search query using linear support vector regression implementation sklearn.SVM.LinearSVR.
* (2) classification of flight itinerary search results by likelihood the returned fare will ultimately be purchased by the consumer, using simple Naive Bayes implementation in sklearn.

## Usage
1. Create a virtual environment and activate it.
```bash
python3 -m venv .venv
source .venv/bin/activate
```
2. Install all needed packages.
```bash
python3 -m pip install -r requirements.txt
```
3. Open one of the notebooks
```bash
jupyter notebook
```